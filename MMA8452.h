/*************************************************************************
 This is a library for the MMA8452 accelerometer. The sensor uses I2C to communicate with the microcontroller. 
 
 Put this file and the MMA8452.cpp file in a folder named "MMA8452", and put this folder inside the libraries
 folder of the Arduino install directory.
 
 Written by Kasun Somaratne for Kasun Somaratne, but you can use it too!
 
 Some of the code bits are adopted from these awesome people:
	*The original MMA8452_n0m1 library: https://github.com/n0m1/MMA8453_n0m1
		-Noah Shibley, NoMi Design Ltd. http://socialhardware.net       
		-Michael Grant, Krazatchu Design Systems. http://krazatchu.ca/
	*MMA8452Q Basic Example Code: https://github.com/dmcinnes/acceleromocity/blob/master/accel.ino	
		-Nathan Seidle, SparkFun Electronics
*************************************************************************/
#ifndef __MMA8452__
#define __MMA8452__

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif
#include "Wire.h"

// The SparkFun breakout board defaults to 1, set to 0 if SA0 jumper on the bottom of the board is set
#define MMA8452_ADDRESS 				0x1D //0X1D if SA0 is HIGH, 0X1C if LOW
#define MMA8452_ID						0x2A

#define MMA8452_SCALEFACTOR_2G			(1024)
#define MMA8452_SCALEFACTOR_4G			(512)
#define MMA8452_SCALEFACTOR_8G			(256)

class MMA8452
{
	public:
		typedef enum
		{											//DEFAULT		TYPE		CONTENT
			REG_STATUS 					= 0x00, 	//00000000		r 			Real time status
			REG_OUT_X_MSB 				= 0x01, 	//				r 			[7:0] are 8 MSBs of 10-bit sample
			REG_OUT_X_LSB 				= 0x02, 	//				r 			[7:6] are 2 LSBs of 10-bit sample
			REG_OUT_Y_MSB 				= 0x03, 	//				r	 		[7:0] are 8 MSBs of 10-bit sample
			REG_OUT_Y_LSB 				= 0x04, 	//				r			[7:6] are 2 LSBs of 10-bit sample
			REG_OUT_Z_MSB 				= 0x05, 	//				r 			[7:0] are 8 MSBs of 10-bit sample
			REG_OUT_Z_LSB 				= 0x06, 	//				r 			[7:6] are 2 LSBs of 10-bit sample
			REG_SYSMOD 					= 0x0b, 	//00000000		r 			Current system mode
			REG_INT_SOURCE 				= 0x0c, 	//00000000		r 			Interrupt status
			REG_WHO_AM_I 				= 0x0d, 	//00111010		r			Factory programmed Device ID
			REG_XYZ_DATA_CFG 			= 0x0e, 	//00000000		r/w			Dynamic range settings. Default is 2g
			REG_HP_FILTER_CUTOFF 		= 0x0f, 	//00000000		r/w 		High pass filter cut-off frequency is set to 16Hz @ 800Hz data rate
			REG_PL_STATUS 				= 0x10, 	//00000000		r 			Landscape/Portrait orientation status
			REG_PL_CFG 					= 0x11, 	//10000000		r/w 		Landscape/Portrait configuration
			REG_PL_COUNT 				= 0x12, 	//00000000		r	 		Landscape/Portrait debounce counter
			REG_PL_BF_ZCOMP 			= 0x13, 	//01000100		r 			Back-Front, Z-Lock trip threshold
			REG_P_L_THS_REG 			= 0x14, 	//10000100		r 			Portrait to Landscape trip angle is 29 degree
			REG_FF_MT_CFG 				= 0x15, 	//00000000		r/w			Freefall/motion functional block configuration
			REG_FF_MT_SRC 				= 0x16, 	//00000000		r			Freefall/motion event source register
			REG_FF_MT_THS 				= 0x17, 	//00000000		r/w			Freefall/motion threshold register
			REG_FF_MT_COUNT 			= 0x18, 	//00000000		r/w 		Freefall/motion debounce counter
			REG_TRANSIENT_CFG 			= 0x1d,		//00000000		r/w 		Transient functional block configuration
			REG_TRANSIENT_SRC 			= 0x1e, 	//00000000		r		 	Transient event status register
			REG_TRANSIENT_THS 			= 0x1f, 	//00000000		r/w 		Transient event threshold
			REG_TRANSIENT_COUNT 		= 0x20,		//00000000		r/w 		Transient debounce counter
			REG_PULSE_CFG 				= 0x21, 	//00000000		r/w			ELE, Double_XYZ or Single_XYZ
			REG_PULSE_SRC 				= 0x22, 	//00000000		r 			EA, Double_XYZ or Single_XYZ
			REG_PULSE_THSX 				= 0x23, 	//00000000		r/w 		X pulse threshold
			REG_PULSE_THSY 				= 0x24, 	//00000000		r/w 		Y pulse threshold
			REG_PULSE_THSZ 				= 0x25, 	//00000000		r/w 		Z pulse threshold
			REG_PULSE_TMLT 				= 0x26, 	//00000000		r/w 		Time limit for pulse
			REG_PULSE_LTCY 				= 0x27, 	//00000000		r/w 		Latency time for 2nd pulse
			REG_PULSE_WIND 				= 0x28, 	//00000000		r/w 		Window time for 2nd pulse
			REG_ASLP_COUNT 				= 0x29, 	//00000000		r/w 		Counter setting for auto-sleep
			REG_CTRL_REG1 				= 0x2a, 	//00000000		r/w 		ODR = 800 Hz, STANDBY mode
			REG_CTRL_REG2 				= 0x2b, 	//00000000		r/w 		Sleep enable, OS Modes, RST, Self Test
			REG_CTRL_REG3 				= 0x2c, 	//00000000		r/w 		Wake from sleep, IPOL, PP_OD
			REG_CTRL_REG4 				= 0x2d, 	//00000000		r/w 		Interrupt enable register
			REG_CTRL_REG5 				= 0x2e, 	//00000000		r/w 		Interrupt pin (INT1/INT2) map
			REG_OFF_X 					= 0x2f, 	//00000000		r/w 		X-axis offset adjust
			REG_OFF_Y 					= 0x30, 	//00000000		r/w 		Y-axis offset adjust
			REG_OFF_Z 					= 0x31	 	//00000000		r/w 		Z-axis offset adjust
		}mma8452Registers_t;
		
		typedef enum
		{
			MMA8452_RANGE_2G,
			MMA8452_RANGE_4G,
			MMA8452_RANGE_8G
		}mma8452Range_t;
		
		typedef enum
		{
			X_AXIS,
			Y_AXIS,
			Z_AXIS
		}mma8452Axis_t;	

		typedef enum
		{
			HPF_LOWEST,
			HPF_LOW,
			HPF_HIGH,
			HPF_HIGHEST
		}mma8452Hpf_t;
		
		typedef struct mma8452Data_s
		{
			float x;
			float y;
			float z;
		}mma8452Data;
		
		MMA8452(void);
		
		bool begin(mma8452Range_t rng=MMA8452_RANGE_2G, byte addr=MMA8452_ADDRESS);
		void update(void);
		void setMotionDetection(mma8452Axis_t axis=X_AXIS, byte threshold=64, byte debounce=0);
		void setTransientDetection(mma8452Axis_t axis=X_AXIS, mma8452Hpf_t hpfcutoff=HPF_HIGHEST, byte threshold=64, byte debounce=0);
		bool readValue(void);
		void writeValue(mma8452Registers_t reg, byte value);
		void hpfilter(bool onStatus=false, mma8452Hpf_t hpfcutoff=HPF_LOWEST);
		
		mma8452Data data; // Last read values are available here
		
	private:
		byte address;
		mma8452Range_t range;
		void standby(void);
		void active(void);
		byte readRegister(byte addressToRead);
		void readRegisters(byte addressToRead, int bytesToRead, byte *dest);
		void writeRegister(byte addressToWrite, byte dataToWrite);
		void readAccelData(int *dest);		
};

#endif