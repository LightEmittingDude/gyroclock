/**
  GyroClock.ino
  
  Purpose: Display time using a moving array of 7 LEDs when triggered by a MMA8452Q 
           accelerometer. Time setting feature using gesture recognition.  
           
  Note: This code is for ATmega328p with internal 8 MHz clock.
        Implements the RTC clock module with a 32.768 kHz crystal.
        Unused pins configured as inputs with internal pull-ups.
  
  @author Kasun Somaratne
  @version 15 March 14, 2015
  
  pin assignments
  PORT  PIN  WHAT  
  BO    14   LED1
  B1    15   LED2
  B2    16   LED3
  B3    17   LED4
  B4    18   LED5
  B5    19   LED6
  C0    23   LED7
  D6    12   TIME_MODE_PIN
  D7    13   DSP_MODE_PIN
  */

#include <avr/sleep.h>  
#include <Wire.h>
#include <MMA8452.h>

#define  TIME_SET_PIN    6
#define  DSP_PIN         7
#define  PIXEL_DELAY     500
#define  THRESHOLD       1.2
#define  DEBOUNCE        5
#define  ACCEL_DELAY     5
#define  DISPLAY_DELAY   0

//an array to store the LED sequence for digits 0-9 and ':'
const byte numarray[] = {B00111110,B01000101,B01001001,B01010001,B00111110,B00000000,B01000000,B01111111,B01000001,B00000000,B01000110,B01001001,B01010001,B01100001,B01000010,B00110110,B01001001,B01000001,B01000001,B00100010,B00010000,B01111111,B00010010,B00010100,B00011000,B00111001,B01000101,B01000101,B01000101,B00100111,B00110010,B01001001,B01001001,B01001001,B00111110,B00000011,B00000101,B01111001,B00000001,B00000001,B00110110,B01001001,B01001001,B01001001,B00110110,B00111110,B01001001,B01001001,B01001001,B00100110,B00000000,B00000000,B00010100,B00000000,B00000000};

boolean accelDSPReady = false;
boolean accelTimeSetReady = false;
boolean minSet = false;
boolean hourSet = false;
boolean checkEnable = false;
float prevData = 0.0;
float maxData = 0.0;
char debCounter = 0;
byte hourCount = 0x00;
byte minCount = 0x00;

char seconds = 0;
char minutes = 0;
char hours = 0;

MMA8452 accel;

void setup()
{
  //define pin modes and initial states
  DDRB |= 0x3F;   // configure PORT B pins 0-5 as outputs
  DDRC |= 0x01;   // configure PORT C pin 0 as output
  DDRC &= ~(0x0E); // configure PORT C pins 1-3 as inputs
  PORTC |= 0x0E;   // enable internal pull-up on PORT C pins 1-3
  DDRD &= ~(0xF7); // configure PORT D pins 0-2, 4-7 as inputs
  PORTD |= 0x37;   // enable internal pull-up on PORT D pins 0-2, 4-5
 
  //initialize real time clock
  initClock();
  //apply power save features
  powerSave();
}

void loop()
{
  //first check which mode we are in
  if(digitalRead(DSP_PIN) == HIGH)
  {
    //configure accelerometer for display mode if not already done so
    if(!accelDSPReady)
    {
      delay(100);
      configAccelDSP();
    }
    else
    {
      accel.update();
      //if minimum checking is enabled
      if(checkEnable)
      {
        //when the data transitions from a negative slope to a positive slope
        if(accel.data.y > prevData)
        {
          displayMode();
          debCounter = 0;
          checkEnable = false;
          maxData = accel.data.y;
        }
      }
      //if pre-condition is satisfied enable minimum checking
      else if((maxData - accel.data.y) > THRESHOLD && (debCounter >= DEBOUNCE))
      {
        checkEnable = true;
      }
      else if((maxData - accel.data.y) > THRESHOLD && (debCounter) < DEBOUNCE)
      {
        debCounter++;
      }
      else if(accel.data.y > maxData)
      {
        maxData = accel.data.y;
      }
       //store the current accel value for comparison with the next value
       prevData = accel.data.y;
       delay(ACCEL_DELAY);
    }
  }
  else if(digitalRead(TIME_SET_PIN) == HIGH)
  {
    //configure accelerometer for time set mode if not already done so
    if(!accelTimeSetReady)
    {
      configAccelTimeSet();
    }
    else if(!minSet)
    {
      if(!hourSet)
      {
         accel.update();
         //if x-axis is pointing at the ground
         if(accel.data.x < -0.9)
         {
           hourSet = true;
           showSignal();
         }
         //if x-axis is not pointing at the ground
         else if(accel.data.x > -0.7)
         {
           hourCount += 1;
           if(hourCount > 23)
           {
             hourCount = 0;
           }
           //show the counter value in binary form
           displayCount(hourCount);
         }
         delay(250);
      }
      else
      {
        accel.update();
        if(accel.data.x < -0.9)
        {
          //if both the minutes and the hours are read, update the time variables
          minSet = true;
          minutes = minCount;
          hours = hourCount;
          //show a signal to indicate that time has been updated
          showSignal();
          //goto sleep since we have set the time and nothing else to do
          sleepNow();
        }
        else if(accel.data.x > -0.7)
        {
          minCount += 1;
          if(minCount > 59)
          {
            minCount = 0;
          }
          displayCount(minCount);
        }
        delay(250);
      }
    }
  }
  else
  {
      //if it is not in display mode or teh time set mode, there is nothing to do but sleep
      minSet = false;
      hourSet = false;
      minCount = 0;
      hourCount = 0;
      accelDSPReady = false;
      accelTimeSetReady = false;
      sleepNow();
  } 
}

//initialize the real time clock (RTC) module
void initClock()
{
  //disable Timer2 interrupts
  TIMSK2 = 0;
  //enable asynchronous mode
  ASSR = (1<<AS2);
  //set initial counter value
  TCNT2 = 0;
  //set prescaller to 64
  TCCR2B |= (1<<CS22);
  //wait for registers to update
  while(ASSR & ((1<<TCN2UB)|(1<<TCR2BUB)));
  //clear interrupt flags
  TIFR2 = (1<<TOV2);
  //enable overflow interrupt
  TIMSK2 = (1<<TOIE2);
  //enable global interrupt
  sei();
}

//enable power saving features and configure the sleep mode
void powerSave()
{
  //shutdown ADC
  PRR |= (1<<PRADC);
  //shutdown analog comparator
  ACSR |= (1<<ACD);
  //turn off watchdog timer
  cli();
  MCUSR &= ~(1<<WDRF);
  WDTCSR |= (1<<WDCE) | (1<<WDE);
  WDTCSR = 0x00;
  sei();
  //configure sleep mode to power-save
  SMCR |= (1<<SM1)|(1<<SM0)|(1<<SE);
}

void sleepNow()
{
  //turn off the brown-out detector before going to sleep. This saves a bit more current.
  MCUCR |= ((1<<BODS)|(1<<BODSE));
  MCUCR &= ~(1<<BODSE);
  sleep_mode();
}

//Timer2 overflow interrupt vector
ISR(TIMER2_OVF_vect)
{
  seconds++;
  if(seconds == 60)
  {
    minutes++;
    seconds = 0;
    if(minutes == 60)
    {
      hours++;
      minutes = 0;
      if(hours == 24)
      {
        hours = 0; 
      }
    }
  }
}

void displayMode()
{
  //gather the digits to be displayed
  char digits[8] = {seconds%10, seconds/10, 10, minutes%10, minutes/10, 10, hours%10, hours/10};
  
  //display each digit one row of pixels at a time. 
   for(int digit = 0; digit < 8; digit++)
   {
     for(int col = digits[digit]*5; col < (digits[digit]+1)*5; col++)
     {
       byte num = numarray[col];
       PORTC |= (num>>6);
       PORTB = num;    
       delayMicroseconds(PIXEL_DELAY);
       PORTC &= ~(0x01);
       PORTB = 0x00;       
     }
     //add a delay between the digits
     delayMicroseconds(4*PIXEL_DELAY);
    }
}

//configure accelerometer for the display mode
void configAccelDSP()
{
  delay(100);
  //set range to +/- 8g
  accel.begin(accel.MMA8452_RANGE_8G);
  //enable high-pass filter with a cutoff frequency of 4 Hz 
  //accel.hpfilter(true,accel.HPF_LOW);
  //enable high-pass filtered data for reading
  accel.writeValue(accel.REG_XYZ_DATA_CFG, 0x02);
  
  accelDSPReady = true;
  accelTimeSetReady = false;
}

//configure accelerometer for time set mode
void configAccelTimeSet()
{
  delay(100);
  //set range for +/- 8g
  accel.begin(accel.MMA8452_RANGE_8G);
  //bypass high-pass filter when reading accelerometer data
  accel.writeValue(accel.REG_XYZ_DATA_CFG, 0x02);
  accelTimeSetReady = true;
  accelDSPReady = false;
}

//in time set mode, used to show a signal that the counter value has been recorded.
void showSignal()
{
  PORTB |= 0x3F;
  PORTC |= 0x01;
  delay(500);
  PORTB &= ~(0x3F);
  PORTC &= ~(0x01);
}

//in time set mode, display the counter value in binary form
void displayCount(byte dspCount)
{
  //PORTC |= (dspCount>>6);
  PORTB |= dspCount;
  delay(250);
  //PORTC &= ~(0x01);
  PORTB &= ~(0x3F);
}

