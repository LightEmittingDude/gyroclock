/*************************************************************************
 This is a library for the MMA8452 accelerometer.
 
 The sensor uses I2C to communicate with the microcontroller. 
 
 Written by Kasun Somaratne for Kasun Somaratne, but you can use it too!
 
 Some of the code bits are adopted from these awesome people:
	*The original MMA8452_n0m1 library: https://github.com/n0m1/MMA8453_n0m1
		-Noah Shibley, NoMi Design Ltd. http://socialhardware.net       
		-Michael Grant, Krazatchu Design Systems. http://krazatchu.ca/
	*MMA8452Q Basic Example Code: https://github.com/dmcinnes/acceleromocity/blob/master/accel.ino	
		-Nathan Seidle, SparkFun Electronics
*************************************************************************/

#include <MMA8452.h>

/***********************************************************************
 CONSTRUCTOR
 ***********************************************************************/

MMA8452::MMA8452(void)
{

}

bool MMA8452::begin(mma8452Range_t rng, byte addr)
{
	Wire.begin();
	
	address = addr;
	range = rng;
	
	//Make sure we have the correct chip ID
	byte id = readRegister(REG_WHO_AM_I);
	if(id != MMA8452_ID)
	{
		return false;
	}
	
	//Initialize data variables
	data.x = 0.0;
	data.y = 0.0;
	data.z = 0.0;
	
	//Must be in standby mode to change registers
	standby();
	
	//Adjust scale
	switch(range)
	{
		case MMA8452_RANGE_2G:
			writeRegister(REG_XYZ_DATA_CFG, 0x00);
			break;
		case MMA8452_RANGE_4G:
			writeRegister(REG_XYZ_DATA_CFG, 0x01);
			break;
		case MMA8452_RANGE_8G:
			writeRegister(REG_XYZ_DATA_CFG, 0x02);
			break;
	}
	
	//Set to active mode to start reading data
	active();
	
	return true;
}

/***********************************************************************
 PUBLIC FUNCTIONS
 ***********************************************************************/
 
 void MMA8452::update(void)
 {
	//Store the 12-bit accel values
	int accelCount[3];
	
	//Read the x/y/z accel data
	readAccelData(accelCount);
	
	//Turn the accel values into actual g's
	float accelG[3]; //Store the actual g's
	
	switch(range)
	{
		case MMA8452_RANGE_2G:
			accelG[0] = (float) accelCount[0] / MMA8452_SCALEFACTOR_2G;
			accelG[1] = (float) accelCount[1] / MMA8452_SCALEFACTOR_2G;
			accelG[2] = (float) accelCount[2] / MMA8452_SCALEFACTOR_2G;
			break;
		case MMA8452_RANGE_4G:
			accelG[0] = (float) accelCount[0] / MMA8452_SCALEFACTOR_4G;
			accelG[1] = (float) accelCount[1] / MMA8452_SCALEFACTOR_4G;
			accelG[2] = (float) accelCount[2] / MMA8452_SCALEFACTOR_4G;
			break;
		case MMA8452_RANGE_8G:
			accelG[0] = (float) accelCount[0] / MMA8452_SCALEFACTOR_8G;
			accelG[1] = (float) accelCount[1] / MMA8452_SCALEFACTOR_8G;
			accelG[2] = (float) accelCount[2] / MMA8452_SCALEFACTOR_8G;
			break;
	}
	
	//Use a rolling filter
	data.x = 0.75 * accelG[0] + data.x * 0.25;
	data.y = 0.75 * accelG[1] + data.y * 0.25;
	data.z = 0.75 * accelG[2] + data.z * 0.25;
	//data.x = accelG[0];
	//data.y = accelG[1];
	//data.z = accelG[2];
 }
 
 void MMA8452::setMotionDetection(mma8452Axis_t axis, byte threshold, byte debounce)
 {
	//Put the device in standby mode
	standby(); 
	
	//Set configuration register for motion detection with selected axis
	byte cfg;
	switch(axis)
	{
		case X_AXIS:
			cfg = 0x48;
			break;
		case Y_AXIS:
			cfg = 0x50;
			break;
		case Z_AXIS:
			cfg = 0x60;	
			break;
	}
	writeRegister(REG_FF_MT_CFG, cfg);
	
	//Set the threshold value for motion detection
	byte ths = threshold;
	if(threshold > 127)
	{
		ths = 0x7F;
	}
	else if(threshold < 0)
	{
		ths = 0x00;
	}
	
	writeRegister(REG_FF_MT_THS, ths);
	
	//Set the debounce count value
	writeRegister(REG_FF_MT_COUNT, debounce);
	
	//Enable motion/freefall interrupt function in system
	byte c_reg4 = readRegister(REG_CTRL_REG4);
	writeRegister(REG_CTRL_REG4, c_reg4 | 0x04);
	
	//Route motion/freefall interrupt function to INT1 hardware pin
	byte c_reg5 = readRegister(REG_CTRL_REG5);
	writeRegister(REG_CTRL_REG5, c_reg5 | 0x04);
	
	//Put the device in Active mode
	active();
 }
 
 void MMA8452::setTransientDetection(mma8452Axis_t axis, mma8452Hpf_t hpfcutoff, byte threshold, byte debounce)
 {
	//Put the device in standby mode
	standby();
	
	//Set selected high pass filter cutoff
	byte hpf;
	switch(hpfcutoff)
	{
		case HPF_HIGHEST:
			hpf = 0x00;
			break;
		case HPF_HIGH:
			hpf = 0x01;
			break;
		case HPF_LOW:
			hpf = 0x02;
			break;
		case HPF_LOWEST:
			hpf = 0x03;
			break;
	}
	writeRegister(REG_HP_FILTER_CUTOFF, hpf);
	
	//Set configuration register for transient detection with selected axis
	byte cfg;
	switch(axis)
	{
		case X_AXIS:
			cfg = 0x02;
			break;
		case Y_AXIS:
			cfg = 0x04;
			break;
		case Z_AXIS:
			cfg = 0x08;
			break;
	}
	writeRegister(REG_TRANSIENT_CFG, cfg);
	
	//Set the threshold value for transient detection
	byte ths = threshold;
	if(threshold > 127)
	{
		ths = 0x7F;
	}
	else if(threshold < 0)
	{
		ths = 0x00;
	}
	writeRegister(REG_TRANSIENT_THS, ths);
	
	//Set the debounce count value
	writeRegister(REG_TRANSIENT_COUNT, debounce);
	
	//Enable transient detection interrupt in the system
	byte c_reg4 = readRegister(REG_CTRL_REG4);
	writeRegister(REG_CTRL_REG4, c_reg4 | 0x20);
	
	//Route transient detection interrupt function to INT1 hardware pin
	byte c_reg5 = readRegister(REG_CTRL_REG5);
	writeRegister(REG_CTRL_REG5, c_reg5 | 0x20);
	
	//Put the device in Active mode
	active();
 }
 
 void MMA8452::hpfilter(bool onStatus, mma8452Hpf_t hpfcutoff)
 {
	//Put the device in Standby mode
	standby();
	
	//Turn on HPF
	if(onStatus)
	{
		writeRegister(REG_XYZ_DATA_CFG, 0x12);
	}
	//Turn off HPF
	else
	{
		writeRegister(REG_XYZ_DATA_CFG, 0x02);
	}
	
	//Set selected high pass filter cutoff
	byte hpf;
	switch(hpfcutoff)
	{
		case HPF_HIGHEST:
			hpf = 0x00;
			break;
		case HPF_HIGH:
			hpf = 0x01;
			break;
		case HPF_LOW:
			hpf = 0x02;
			break;
		case HPF_LOWEST:
			hpf = 0x03;
			break;
	}
	writeRegister(REG_HP_FILTER_CUTOFF, hpf);
	
	//Put the device in Active mode
	active();
 }
 
 bool MMA8452::readValue(void)
 {
	byte c_int_src = readRegister(REG_INT_SOURCE);
	if(c_int_src == 0x20)
	{
		byte c_tran_src = readRegister(REG_TRANSIENT_SRC);
		//return true;
		if(c_tran_src == 0x4C)
		{
			return true;
		}
	}
	return false;
 }
 
 void MMA8452::writeValue(mma8452Registers_t reg, byte value)
 {
	//Put the device in standby mode to change registers
	standby();
	writeRegister(reg, value);
	//Put the device in Active mode
	active();
 }
 
 
 /************************************************************************
  PRIVATE FUNCTIONS
  ************************************************************************/
  
 void MMA8452::standby(void)
 {
	byte val = readRegister(REG_CTRL_REG1);
	writeRegister(REG_CTRL_REG1, val & ~(0x01)); //Clear the active bit to go into standby mode
 }
 
 void MMA8452::active(void)
 {
	byte val = readRegister(REG_CTRL_REG1);
	writeRegister(REG_CTRL_REG1, val | 0x01); //Set the active bit to begin detection
 }
  
 void MMA8452::readAccelData(int *dest)
 {
	//Store x,y,z accel register data
	byte rawData[6];
	
	//Read the six data registers with accel values for each axis
	readRegisters(REG_OUT_X_MSB, 6, rawData);
	
	//Loop to combine the 8-bit MSB and LSB bits into a 12-bit number
	for(int i = 0; i < 3; i++)
	{
		int gCount = (rawData[i*2] << 8) | rawData[(i*2)+1];
		gCount >>= 4; //right align the numbers
		
		//If number is negative, manually change it since no 12-bit data type
		if(rawData[i*2] > 0x7F)
		{
			gCount = ~gCount + 1; //Transformed into negative 2's complement #
			gCount *= -1;
		}
		
		dest[i] = gCount;	
	}
 }
 
 byte MMA8452::readRegister(byte addressToRead)
 {
	Wire.beginTransmission(MMA8452_ADDRESS);
	Wire.write(addressToRead);
	Wire.endTransmission(false); //End Transmission but keep the connection active (repeated start)
	
	Wire.requestFrom(MMA8452_ADDRESS, 1); //Ask for 1 byte, once done bus is released by default
	
	while(!Wire.available()); //Wait for data to come back
	return Wire.read(); //Return the received byte
 }
 
 void MMA8452::readRegisters(byte addressToRead, int bytesToRead, byte *dest)
 {
	Wire.beginTransmission(MMA8452_ADDRESS);
	Wire.write(addressToRead);
	Wire.endTransmission(false); //End Transmission but keep the connection active (repeated start)
	
	Wire.requestFrom(MMA8452_ADDRESS, bytesToRead); //Ask for bytes, once done bus is released by dafault
	
	while(Wire.available() < bytesToRead); //Wait until the requested number of bytes is received
	
	for(int i = 0; i < bytesToRead; i++)
	{
		dest[i] = Wire.read();
	}
 }
 
 void MMA8452::writeRegister(byte addressToWrite, byte dataToWrite)
 {
	Wire.beginTransmission(MMA8452_ADDRESS);
	Wire.write(addressToWrite);
	Wire.write(dataToWrite);
	Wire.endTransmission(); //End transmission
 }
 